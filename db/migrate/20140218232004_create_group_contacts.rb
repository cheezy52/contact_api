class CreateGroupContacts < ActiveRecord::Migration
  def change
    create_table :group_contacts do |t|
      t.integer :group_id, :references => :groups
      t.integer :contact_id, :references => :contacts

      t.timestamps
    end
  end
end
