class CreateContactShares < ActiveRecord::Migration
  def change
    create_table :contact_shares do |t|
      t.integer :contact_id, :references => :contacts
      t.integer :user_id, :references => :users

      t.timestamps
    end
    add_index :contact_shares, [:user_id, :contact_id], :unique => true
  end
end
