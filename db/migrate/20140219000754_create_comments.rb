class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer :user_id, :references => :users
      t.integer :contact_id, :references => :contacts
      t.text :text

      t.timestamps
    end
  end
end
