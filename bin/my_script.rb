require 'addressable/uri'

url = Addressable::URI.new(
  scheme: 'http',
  host: 'localhost',
  port: 3000,
  path: '/users/1/contacts/1/comments',
).to_s


puts RestClient.post(url, :comment => {:text => "what a cool guy", :user_id => 2, :contact_id => 1 } )