# == Schema Information
#
# Table name: contacts
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  email      :string(255)
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

class Contact < ActiveRecord::Base
  validates :name, :email, :user_id, :presence => true
  validates :email, :uniqueness => true

  belongs_to :owner,
  :primary_key => :id,
  :foreign_key => :user_id,
  :class_name => "User"

  has_many :shares,
  :primary_key => :id,
  :foreign_key => :contact_id,
  :class_name => "ContactShare"

  has_many :shared_users,
  :through => :shares,
  :source => :user

  has_many :group_contacts

  has_many :groups,
  :through => :group_contacts,
  :source => :group

  has_many :comments

  def self.contacts_for_user_id(user_id)
    query = <<-SQL
    SELECT DISTINCT
      contacts.*
    FROM
      contacts
    LEFT OUTER JOIN
      contact_shares
    ON
      contacts.id = contact_shares.contact_id
    WHERE
      contacts.user_id = ?
    OR
      contact_shares.user_id = ?
    SQL
    self.find_by_sql [query, user_id, user_id]
  end

  def self.favorites(user_id)
    query = <<-SQL
    SELECT DISTINCT
      contacts.*
    FROM
      contacts
    LEFT OUTER JOIN
      contact_shares
    ON
      contacts.id = contact_shares.contact_id
    WHERE
      (contacts.user_id = ?
    OR
      contact_shares.user_id = ?)
    AND
      contact_shares.favorite = ?
    SQL
    self.find_by_sql [query, user_id, user_id, true]
  end

end
