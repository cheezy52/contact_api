class Group < ActiveRecord::Base
  validates :name, :presence => true, :uniqueness => true
  validates :user_id, :presence => true

  belongs_to :user

  has_many :group_contacts,
  :primary_key => :id,
  :foreign_key => :group_id,
  :class_name => "GroupContact"

  has_many :contacts,
  :through => :group_contacts,
  :source => :contact

end
