class Comment < ActiveRecord::Base
  validates :user_id, :contact_id, :text, :presence => true

  belongs_to :user
  belongs_to :contact
end
