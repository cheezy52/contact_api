class GroupsController < ApplicationController
  def index
    render :json => Group.where(:user_id => params[:user_id])
  end

  def show
    render :json => Group.find(params[:id]).contacts
  end

  def create
    @group = Group.new(params[:group].permit(:name, :user_id))
    if @group.save
      render :json => @group
    else
      render :json => @group.errors.full_messages, :status => 418
    end
  end

  def update
    @group = Group.find(params[:id])
    @group.update_attributes(params[:group].permit(:name))
    if @group.save
      render :json => @group
    else
      render :json => @group.errors.full_messages, :status => 418
    end
  end

  def destroy
    @group = Group.find(params[:id])
    @group.destroy unless @group.nil?
    render :json => @group
  end
end
