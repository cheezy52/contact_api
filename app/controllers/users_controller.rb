class UsersController < ApplicationController
  def index
    @users = User.all
    render :index
  end

  def show
    @user = User.find(params[:id])
    render :json => @user
  end

  def create
    puts "WE ARE IN CREATE!!!"
    p params
    @user = User.new(user_params)
    if @user.save
      render :json => @user
    else
      render :json => @user.errors.full_messages, :status => 418
    end
  end

  def update
    @user = User.find(params[:id])
    @user.update_attributes(user_params)
    if @user.save
      render :json => @user
    else
      render :json => @user.errors.full_messages, :status => 418
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy unless @user.nil?
    render :json => @user
  end

  private
  def user_params
    params[:user].permit(:username)
  end
end
