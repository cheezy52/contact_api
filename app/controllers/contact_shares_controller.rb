class ContactSharesController < ApplicationController
  def create
    @contact_share = ContactShare.new(share_params)
    if @contact_share.save
      render :json => @contact_share
    else
      render :json => @contact_share.errors.full_messages, :status => 418
    end
  end

  def update
    @contact_share = ContactShare.find(params[:id])
    @contact_share.update_attributes(params[:contact_share].permit(:favorite))
    if @contact_share.save
      render :json => @contact_share
    else
      render :json => @contact_share.errors.full_messages, :status => 418
    end
  end

  def destroy
    @contact_share = ContactShare.find(params[:id])
    @contact_share.destroy unless @contact_share.nil?
    render :json => @contact_share
  end

  private
  def share_params
    params[:contact_share].permit(:user_id, :contact_id)
  end
end
