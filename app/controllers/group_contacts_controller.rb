class GroupContactsController < ApplicationController
  def create
    @group_contact = GroupContact.new(params[:group_contact].permit(:group_id, :contact_id))
    if @group_contact.save
      render :json => @group_contact
    else
      render :json => @group_contact.errors.full_messages, :status => 418
    end
  end

  def destroy
    @group_contact = GroupContact.find(params[:id])
    @group_contact.destroy unless @group_contact.nil?
    render :json => @group_contact
  end
end
