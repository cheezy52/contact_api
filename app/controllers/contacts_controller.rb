class ContactsController < ApplicationController
  def index
    @user = User.find(params[:user_id])
    @contacts = Contact.contacts_for_user_id(params[:user_id])
    @favorites = self.favorites
    render :index
  end

  def show
    @contact = Contact.find(params[:id])
    render :json => @contact
  end

  def create
    @contact = Contact.new(contact_params)
    if @contact.save
      render :json => @contact
    else
      render :json => @contact.errors.full_messages, :status => 418
    end
  end

  def update
    @contact = Contact.find(params[:id])
    @contact.update_attributes(contact_params)
    if @contact.save
      render :json => @contact
    else
      render :json => @contact.errors.full_messages, :status => 418
    end
  end

  def destroy
    @contact = Contact.find(params[:id])
    @contact.destroy unless @contact.nil?
    render :json => @contact
  end

  def favorites
    Contact.favorites(params[:user_id])
  end

  def favorite
    @contact_share = ContactShare.find_by_contact_id_and_user_id(
      params[:contact_id], params[:user_id])
    @contact_share.update_attributes(
      @contact_share.favorite = (params[:contact_share].permit(:favorite)))
    if @contact_share.save
      render :json => @contact_share
    else
      render :json => @contact_share.errors.full_messages, :status => 418
    end
  end

  private
  def contact_params
    params[:contact].permit(:name, :email, :user_id)
  end
end
