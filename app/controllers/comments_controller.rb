class CommentsController < ApplicationController
  def index
    render :json => Comment.where(:user_id => params[:user_id], :contact_id => params[:contact_id])
  end

  def show
    render :json => Comment.find(params[:id])
  end

  def create
    p params
    @comment = Comment.new(params[:comment].permit(:user_id, :contact_id, :text))
    if @comment.save
      render :json => @comment
    else
      render :json => @comment.errors.full_messages, :status => 418
    end
  end

  def update
    @comment = Comment.find(params[:id])
    @comment.update_attributes(params[:comment].permit(:text))
    if @comment.save
      render :json => @comment
    else
      render :json => @comment.errors.full_messages, :status => 418
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy unless @comment.nil?
    render :json => @comment
  end
end
